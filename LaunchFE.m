(*****************************************************************)

Needs["JLink`"]
(* this is the program the JLink will use to launch a FrontEnd *)
$FrontEndLaunchCommand = FileNameJoin[{$InstallationDirectory, "Mathematica.exe"}]
(* this connects the kernel to a FrontEnd launched via JLink *)
ConnectToFrontEnd[]
(* at this point, FrontEnd is not visible, available only for processing some commands *)
(* this function opens a new Notebook in a Window making the FrontEnd visible *)
UseFrontEnd[
    nbObj = CreateWindow[
                DocumentNotebook[{TextCell["Launched by JVM Kernel", "Title"]}], 
                Visible -> True ]
            ]
(* you should now see an unnamed notebook with first cell as a Title "Launched by JVM Kernel" *)			
