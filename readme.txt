2014.07.25

This example demonstrates the issue I am having connecting a frontend to an already running kernel

Files:
	readme.txt  - this file
	runIt.bat   - windows batch file that will launch a Kernel in a Windows terminal 
	LaunchFE.m  - mathematica expressions to launch a frontend from the kernel via Jlink

Assumptions:
	You have version 10 of Mathematica installed in standard place on Windows
	If you don't then edit runIt.bat setting %mmaInstallationPath% to correct value.
	Example for version 9.0, change:
		set mmaInstallationPath="C:\Program Files\Wolfram Research\Mathematica\10.0"
	to:
		set mmaInstallationPath="C:\Program Files\Wolfram Research\Mathematica\9.0"
	
	
Instructions:
	Just run runIt.bat from a Windows cmd or powershell (.\runIt.bat for Powershell)
	
	runIt.bat launches a kernel in terminal mode and "Gets[]" the launchFE.m file
	
Question:
runIt.bat launches a kernel (call it Kernel A) in terminal mode and "Gets[]" the launchFE.m file
The launchFE.m launches a FrontEnd and opens a new Notebook.
The net result of both commands is that you have Kernel A in terminal mode and a FrontEnd which is
connected to Kernel A (for example we can send it commands using UseFrontEnd[]) but it has its own
kernel for evaluation (call it Kernel B).

I'd like to set the Evaluation kernel of the frontend to Kernel A, the kernel that launched it

I've tried creating a link in Kernel A using:
	feMathLink = LinkCreate["feMathLink"] 
and connecting to this link from frontend using:
	jvmLink = LinkConnect["feMathLink"]
	SetOptions[EvaluationNotebook[], Evaluator -> "feMathLink"] 
	
	